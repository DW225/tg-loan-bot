import loanHandler from './src/loan';
import loanListHandler from './src/list';
import loanReturnHandler from './src/return';
import loanTransfer from './src/transfer';

require('dotenv').config();

const TeleBot = require('telebot');

const bot = new TeleBot(process.env.TELEGRAM_TOKEN);

bot.on('/^\/loan (.+)$/', (msg, props) => {
  const debitorId = props.match?.[1] ?? null;
  const creditorId = msg.from.id;
  const debtTotal = props.match?.[2] ?? null;

  if (debitorId === null || debtTotal === null) {
    bot.reply.text('/loan @欠錢Id 金額');
  }
});

bot.on('/list', (msg) => {
  const chatRoomId = msg.chat.id;
});

// bot.on('/help', (msg) => {});

bot.on('/return', (msg) => {});

// bot.on('/borrow', (msg) => {});

bot.on('/transfer', (msg) => {});

bot.start();
