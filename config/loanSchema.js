const mongoose = require('mongoose');

const { Schema } = mongoose;

const LoanSchema = new Schema({
  Id: Number, // loan id
  CreditorId: String, // 債權人
  DebitorId: String, // 欠錢仔
  DebtTotal: Number, // 總欠債
  DebtLeft: Number, // 未還清金額
  Finished: Boolean, // 是否還清
  Currency: String, // 幣值
  Note: String, // 備註
});

module.exports = mongoose.model('LoanModel', LoanSchema);
