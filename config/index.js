import _ from 'lodash';

require('dotenv').config();

const all = {
  telegram: {
    token: process.env.TELEGRAM_TOKEN || '',
    externalUrl: process.env.EXTERNAL_URL || '',
    port: process.env.PORT || 443,
    host: '0.0.0.0',
  },
  logLevel: 'verbose',
};
const envs = {
  production: {
    logLevel: 'info',

  },
  development: {
    logLevel: 'verbose',

  },
};
export default _.merge(all, envs[process.env.NODE_ENV]);
